package com.springboot.jwt.exception;

import lombok.Getter;

/**
 * @Author: Jyf
 * @Date: 2020/12/17 10:31
 */
@Getter
public class BizException extends RuntimeException {
    protected int errorCode;
    protected String errorMsg;

    public BizException(String msg) {
        super(msg);
        this.errorMsg=msg;
    }

    public BizException(int code,String msg) {
        super(msg);
        this.errorCode=code;
        this.errorMsg=msg;
    }
}
