package com.springboot.jwt.service;

import com.springboot.jwt.pojo.User;

import java.util.List;
import java.util.Optional;

/**
 * @Author: Jyf
 * @Date: 2020/12/17 9:24
 */
public interface UserService {
    /**
     * 查询用户by用户名
     *
     * @param username
     * @return User
     */
    User findByUsername(String username);

    User findById(Long id);

    List<User> findList();

    /**
     * 删除用户
     *
     * @param id
     */
    void deleteById(Long id);


    /**
     * 保存用户
     *
     * @param user
     */
    void add(User user);

    void update(User user);
}
