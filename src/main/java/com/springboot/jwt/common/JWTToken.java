package com.springboot.jwt.common;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Author: Jyf
 * @Date: 2020/12/17 10:47
 */
public class JWTToken implements AuthenticationToken {
    // 密钥
    private String token;

    public JWTToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
