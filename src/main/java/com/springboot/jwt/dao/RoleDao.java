package com.springboot.jwt.dao;

import com.springboot.jwt.pojo.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: Jyf
 * @Date: 2020/12/18 10:57
 */
public interface RoleDao extends JpaRepository<Role, Long> {
}
