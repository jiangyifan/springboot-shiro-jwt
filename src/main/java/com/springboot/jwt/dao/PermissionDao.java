package com.springboot.jwt.dao;

import com.springboot.jwt.pojo.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: Jyf
 * @Date: 2020/12/18 10:57
 */
public interface PermissionDao extends JpaRepository<Permission, Long> {
}
