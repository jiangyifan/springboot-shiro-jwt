package com.springboot.jwt.dao;

import com.springboot.jwt.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: Jyf
 * @Date: 2020/12/17 9:20
 */
public interface UserDao extends JpaRepository<User, Long> {
    /**
     *  查询用户by用户名
     * @param username
     * @return
     */
    User findByUsername(String username);
}
