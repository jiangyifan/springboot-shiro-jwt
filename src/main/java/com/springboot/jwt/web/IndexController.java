package com.springboot.jwt.web;

import com.springboot.jwt.common.Result;
import com.springboot.jwt.common.ResultUtils;
import com.springboot.jwt.dao.RoleDao;
import com.springboot.jwt.exception.BizException;
import com.springboot.jwt.pojo.Role;
import org.apache.shiro.authz.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @Author: Jyf
 * @Date: 2020/12/17 12:04
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    RoleDao roleDao;

    @GetMapping("/a")
    @RequiresRoles("admin")
    public Result test() {
        return ResultUtils.genSuccessResult("Roles:admin 允许访问！");
    }

    @GetMapping("/b")
    @RequiresAuthentication
    public Result Authentication() {
        return ResultUtils.genSuccessResult("Authentication 允许访问！");
    }

    @GetMapping("/c")
    @RequiresRoles(value={"user","admin"}, logical=Logical.OR)
    public Result Roles() {
        return ResultUtils.genSuccessResult("Roles:user or admin 允许访问！");
    }

    @GetMapping("/d")
    @RequiresPermissions("user:hello")
    public Result RequiresPermissions() {
        return ResultUtils.genSuccessResult("RequiresPermissions user:hello 允许访问！");
    }

    @GetMapping("/e")
    @RequiresPermissions("user:update")
    public Result update() {
        return ResultUtils.genSuccessResult("RequiresPermissions user:update 允许访问！");
    }

    @GetMapping("/f")
    @RequiresPermissions("update")
    public Result testF() {
        return ResultUtils.genSuccessResult("*:update 允许访问！");
    }

    @GetMapping("/g")
    @RequiresPermissions("admin:update")
    public Result updateG() {
        return ResultUtils.genSuccessResult("RequiresPermissions admin:update 允许访问！");
    }

    @GetMapping("/h")
    @RequiresPermissions("add")
    public Result updateH() {
        return ResultUtils.genSuccessResult("RequiresPermissions *:add 允许访问！");
    }
}
