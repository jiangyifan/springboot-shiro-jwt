package com.springboot.jwt.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @Author: Jyf
 * @Date: 2020/12/18 10:54
 */
@Entity
@Table(name="role")
@Data
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 主键ID
    private String name; // 角色名
    @Column(name = "permission_id")
    private Long permissionId; // 权限ID
}
