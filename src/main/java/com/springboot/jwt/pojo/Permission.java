package com.springboot.jwt.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @Author: Jyf
 * @Date: 2020/12/18 10:56
 */
@Entity
@Table(name="permission")
@Data
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 主键ID
    private String description; // 权限描述
}
