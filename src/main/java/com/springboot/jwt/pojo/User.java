package com.springboot.jwt.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Jyf
 * @Date: 2020/12/17 9:20
 */
@Entity
@Table(name = "user")
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 主键ID
    private String username; // 用户名
    private String password; // 密码
    private String salt; // 随机盐
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    @Column(name = "create_time")
    private Date creatTime;
    @Column(name = "role_id")
    private Long roleId;
}
