# springboot-shiro-jwt

#### 介绍
Shiro + JWT + Spring Boot Restful 简易教程

#### 软件架构
完全使用了 Shiro 的注解配置，保持高度的灵活性。
放弃 Cookie ，Session ，使用JWT进行鉴权，完全实现无状态鉴权。
JWT 密钥支持过期时间。

#### 程序逻辑

1. 我们 POST 用户名与密码到 /login 进行登入，如果成功返回一个加密 token，失败的话直接返回 401 错误。
2. 之后用户访问每一个需要权限的网址请求必须在 header 中添加 Authorization 字段，例如 Authorization: token ，token 为密钥。
3. 后台会进行 token 的校验，如果有误会直接返回 401。

#### Token加密说明

1.  携带了 username 信息在 token 中。
2.  设定了过期时间。
3.  使用用户登入密码对 token 进行加密。

#### Token校验流程

1.  获得 token 中携带的 username 信息。
2.  进入数据库搜索这个用户，得到他的密码。
3.  使用用户的密码来检验 token 是否正确。